#include "gluchytelefon.h"
#include "2argument.h"

// 2. "Argument"
// Wejście: argument do programu, parsowana za pomocą getopt
// Modyfikacja/wyjscie: następna liczba pierwsza
//stara wersja x:= lustrzane odbicie bitów, np dla 6(110):= 3(011)

int main(int argc, char *argv[])
{
    displayInfo(INTRO, "#####     2     #####   next prime number");
    char buffer[BUFFER_SIZE];

    long int number = parseCmdOption(argc, argv);
    if (!checkRange(number))
    {
        displayInfo(ERROR, TXT_ERROR_INPUT_OUT_OF_RANGE);
        return 0;
    }
    sprintf(buffer, "%ld", number);
    displayInfo(INPUT, buffer);
    //displayBits(number);

    //unsigned int reverse = transform((unsigned int)number);
    //displayBits(reverse);
    
    number = transform(number);
    if (!checkRange(number))
    {
        displayInfo(ERROR, TXT_ERROR_INPUT_OUT_OF_RANGE);
        return 0;
    }

    sprintf(buffer, "%ld", number);
    displayInfo(OUTPUT, buffer);

    send(number);
    return 0;
}

long int parseCmdOption(int argc, char *argv[])
{
    int param;
    long int number = 0;
    while ((param = getopt(argc, argv, "hi:")) != -1)
    {
        switch (param)
        {
        case 'i': // input
            number = atol(optarg); /*return long integer representation of a string, skips all the white-space characters at hte beginning of the string, converts the subsquent characters as part of the number, and then stops when it encounters the first character that isn t a number
*/
            return number;
            break;

        case 'h': // help
        default:
            printf("Usage:\n\t%s -i [0-%u]\n", argv[0], UINT_MAX);
            exit(0);
            break;
        }
    }
    return -1;
}

void send(const unsigned int number)
{
    int fd;
    unlink(FIFO_PATH);/*deletes a name from the filesystem*/
    mkfifo(FIFO_PATH, 0666);// r-4, w - 2, e -1 //utowrzenie fifo

    pid_t pid;
    if ((pid = fork()) == -1)
    {
        perror("Fork error");
        exit(0);
    }
    else
    {
        if (pid == 0) //im child - zapisanie do dziecka
        {
            char *pTxt = NULL;
            asprintf(&pTxt, "%u", number);/*przypisanie numeru jako stringa do pTxt*/
            fd = open(FIFO_PATH, O_WRONLY);/*otwarcie i zabolokwanie do zapisu*/
            if (write(fd, pTxt, sizeof(int) * 8) < 0)
            {
                perror("Couldn't write to FIFO");
                exit(0);
            }
            close(fd); 
            free(pTxt);
            exit(0);
        }
    }

    execl("./3pipe.out", "software", NULL);/*funkcja execl-przekszatałca proces wywołujący przez załadowanie nowego programu do jego przestrzeni pamieci - stary program przykryty przez nowy program, wykonywany nastepnie od samego poczatku - stary program wymazywany przez nowy - ne ma portotu z exec. Execl - wszystkie jej parametry sa wskzniami znakowymi. 1szy parametr - path podaje nazwe pliku zawierajacego program, ktory ma byc wykonywany(wazna nazwa sciezki-wzg lub bezwzgl). 2gi oarametr -arg0 - nazwa programu albo polecenia pozbawiona nazwy sciezki. Ten i pozostala zmienna liczba argumentow(arg1 do argn) sa dostepne dla wywolywanego programu, odpowiadajac argumentom wiersza polecen w powloce 
*/
}

bool isPrime(const unsigned long number) //trialDivision
{
    if(number<=1) return false;
    if(number==2) return true;
    for(unsigned int i=2; (i*i)<=number; ++i)
    {
        if(number%i==0) return false;
    }
    return true;
}

long int transform(long int number)
{
    bool prime=false;
    while(!prime)
    {
        number++;
        if(!checkRange(number)) return -1;
        if(isPrime(number)) prime=true;
    }
    return number;
}
/*unsigned int transform(unsigned int number) // 32 bit reverse
{
    unsigned int size = sizeof(number) * 8;
    unsigned int reverse = 0;

    for (unsigned int i = 1; i <= size; i++)
    {
        reverse |= (number & 1) << (size - i);
        number >>= 1;
    }

    return reverse;
}*/
