#include "gluchytelefon.h"
#include "1konsola.h"

// 1. "Konsola":
// wejście: stdin
// Modyfikacja/wyjscie: x:=x modulo 10

int main()
{
    signal(SIGINT, breakHandler); //SIGNINT - PRZERWANIE PROCESU CTRL+C

    long int number = 0;
    while (1)
    {
        printf("\n\n\n");
        displayInfo(INTRO, "#####     1     #####   x=x % 10");

        printf("Enter number [0-%u]: ", UINT_MAX); //limits.h
        if (scanf("%ld", &number))
        {
            char buffer[BUFFER_SIZE]; 
            if (!checkRange(number))  //spr czy sie miesci
            {
                displayInfo(ERROR, TXT_ERROR_INPUT_OUT_OF_RANGE);
                continue;
            }
            sprintf(buffer, "%ld", number); //spr czy sie miesci
            displayInfo(INPUT, buffer);

            number = transform(number);
            if (!checkRange(number)) //spr czy sie miesci po zmianie wart
            {
                displayInfo(ERROR, TXT_ERROR_OUTPUT_OUT_OF_RANGE);
                continue;
            }
            sprintf(buffer, "%ld", number); //wspisuje wartość numeru do buffer 
            displayInfo(OUTPUT, buffer);

            send((unsigned int)number);
        }
        else
        {
            scanf("%*s");
            displayInfo(ERROR, "Incorrect characters");
        }
    }
    return 0;
}

void breakHandler()
{
    printf("\nShutting down...\n");//gdy ktoś wcisnie Ctrl +C wyswitli sie ten komunikat, poki tego nie zrobi bedzie działało
    exit(0);
}

void send(const unsigned int number)
{
    char *pTxt = NULL;
    asprintf(&pTxt, "./2argument.out -i %u", number); /*wpisuje wartosc numeru do ptxti od razu alokuje na to pamiec-trzeba sprawdzic czy udala sie alokacja jak w mallocku
    if */
    system(pTxt);
    free(pTxt);
}

long int transform(long int number)
{
    return number = number%10;
    //return ++number;
}
