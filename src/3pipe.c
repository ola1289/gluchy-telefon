#include "gluchytelefon.h"
#include "3pipe.h"

// 3. "Pipe"
// Wejście: Nazwany pipe
// Modyfikacja/wyjscie: Ustawienie bita na pozycji okreslonej przez x

int main()
{
    displayInfo(INTRO, "#####     3     #####   setting a bit");
    char buffer[BUFFER_SIZE];

    long int number = receive();
    sprintf(buffer, "%ld", number); //zapis number do buffer
    displayInfo(INPUT, buffer); 
    if(!checkRange(number))
    {
        displayInfo(ERROR, TXT_ERROR_INPUT_OUT_OF_RANGE);
        return 0;
    }
    
    displayBits(number); 
    unsigned int numb_set = transform((unsigned int)number);
    displayBits(numb_set);
    sprintf(buffer, "%u", numb_set);
    displayInfo(OUTPUT, buffer);

    send(numb_set);
    return 0;
}


long int receive()
{
	int fd;
    char buffer[sizeof(int)*8];
	fd=open(FIFO_PATH,O_RDONLY); //otwarcie fifo do odczytu
    if( read(fd,buffer,sizeof(int)*8) < 0 )
    {
        perror("Couldn't read from FIFO");
        exit(0);
    }
	close(fd);
    unlink(FIFO_PATH);

    return atol(buffer); //zmiana char na long int
}

void send(const unsigned int number)
{
    int fd;

    char *pTxt = NULL;
    asprintf(&pTxt, "%u", number);
    fd = open(CHRDEV_PATH,O_WRONLY);
    if(fd<0)
    {
        perror("Failed to open the device CHRDEV");
        exit(0);
    }
    if( write(fd,pTxt,sizeof(int)*8) < 0 )
    {
        perror("Couldn't write to CHRDEV");
        exit(0);
    }
    close(fd);
    free(pTxt);

    execl("./4jajko.out", "software", NULL);
}


unsigned int transform(unsigned int number)
{
    unsigned int *numb_wsk;
    numb_wsk = &number;

    printf("%d\n", number);
    *numb_wsk |= (1 << number);

    unsigned int numb_set = *numb_wsk;
    printf("%d\n", number);

    return numb_set;
}


