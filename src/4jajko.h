#ifndef _4JAJKO_H_
#define _4JAJKO_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>


void inetServer(const unsigned int);
long int receive();
void gt_send(const unsigned int);
unsigned int transform(unsigned int);


#endif
