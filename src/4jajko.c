#include "gluchytelefon.h"
#include "4jajko.h"
//#include "genl.h"

// 4. "Jajko"
// Wejście: interfejs znakowy (linux kernel module over chrdev)
// Modyfikacja/wyjscie: x:= lustrzane odbicie bitów, np. dla 6(110):= 3(011)

int main()
{
    displayInfo(INTRO, "#####     4     #####   bit reverse");
    char buffer[BUFFER_SIZE];

    long int number = receive();
    sprintf(buffer, "%ld", number);
    displayInfo(INPUT, buffer);
    if (!checkRange(number))
    {
        displayInfo(ERROR, TXT_ERROR_INPUT_OUT_OF_RANGE);
        return 0;
    }
    
    displayBits(number);
    unsigned int reverse = transform((unsigned int)number);
    if (!checkRange(reverse))
    {
        displayInfo(ERROR, TXT_ERROR_OUTPUT_OUT_OF_RANGE);
        return 0;
    }
    displayBits(reverse);
    sprintf(buffer, "%u", reverse);
    displayInfo(OUTPUT, buffer);
    
    gt_send(reverse);
    return 0;

}

long int receive()
{
    int fd;
    char buffer[sizeof(int) * 8];
    fd = open(CHRDEV_PATH, O_RDONLY);
    if (fd < 0)
    {
        perror("Failed to open the device CHRDEV");
        exit(0);
    }
    if (read(fd, buffer, sizeof(int) * 8) < 0)
    {
        perror("Couldn't read from CHRDEV");
        exit(0);
    }
    close(fd);

    return atol(buffer);
}

void inetServer(const unsigned int number)
{
    int server_fd, client_fd;
    int backlog = 5;
    struct sockaddr_in server_addr, client_addr;
    char buffer[BUFFER_SIZE];

    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(TCP_PORT); // network byte order

    server_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_fd < 0)
    {
        perror("socket");
        exit(0);
    }

    int yes = 1;
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &yes, sizeof(yes)))
    {
        perror("setsockopt");
        exit(0);
    }
    if (bind(server_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
    {
        perror("bind");
        exit(0);
    }
    if (listen(server_fd, backlog) < 0)
    {
        perror("listen");
        exit(0);
    }

    socklen_t client_addr_len = sizeof(client_addr);
    client_fd = accept(server_fd, (struct sockaddr *)&client_addr, &client_addr_len);
    if (client_fd < 0)
    {
        perror("accept");
        exit(0);
    }

    sprintf(buffer, "%u", number);
    if (send(client_fd, buffer, sizeof(buffer), 0) < 0)
    {
        perror("write");
        exit(0);
    }

    close(client_fd);
    close(server_fd);
}

void gt_send(const unsigned int number)
{
    pid_t pid;
    if ((pid = fork()) == -1)
    {
        perror("Fork error");
        exit(0);
    }
    else
    {
        if (pid == 0)
        {
            inetServer(number);
            exit(0);
        }
    }

    execl("./6siec.out", "software", NULL);
}

unsigned int transform(unsigned int number) 
{
    unsigned int size = sizeof(number) * 8;
    unsigned int reverse = 0;

    for (unsigned int i = 1; i <= size; i++)
    {
        reverse |= (number & 1) << (size - i);
        number >>= 1;
    }

    return reverse;
}
